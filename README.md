# indexer

Projeto para avaliação da disciplina de Estrutura de Dados II

### Alunos

| Nome                       | GRR      |
|----------------------------|----------|
| Caroline Picanço Prockmann | 20224163 |
| Lucas Fracaro Nunes        | 20221125 |

## Getting started

Para executar o programa em um sistema operacional linux basta compilar o arquivo utilizando o compilador gcc (Comum na maioria das distribuições linux). Ou algum outro compilador da sua preferência.

```
gcc -o indexer indexer.c -lm
```

Após isso basta chamar o executável `./indexer` com um das três opções listadas abaixo:

## OPÇÕES

##
```
./indexer --freq-word PALAVRA ARQUIVO
```
Exibe o número de ocorrência das N palavras que mais aparecem em ARQUIVO, em
ordem decrescente de ocorrência.

##

``` 
./indexer --freq N ARQUIVO
```
Exibe o número de ocorrência das N palavras que mais aparecem em ARQUIVO, em
ordem decrescente de ocorrência.

##

```
./indexer --search TERMO ARQUIVO [ARQUIVOS]
```

Exibe uma listagem dos ARQUIVOS mais relevantes encontrados pela busca por 
TERMO. A listagem é apresentada em ordem descrescente de relevância. 
TERMO pode conter mais de uma palavra. Neste caso, deve ser indicado entre 
àspas.
